<?php
/**
 * Plugin Name: WPForms Google Tag Manager Add-On
 * Description: WPForms add-on to capture form submissions and data with Google Tag Manager.
 * Version: 1.2
 * Update URI: https://bitbucket.org/SolutionBuilt/wordpress-plugin-updates/raw/master/info.json
 * Author: SolutionBuilt
 * Author URI: https://www.solutionbuilt.com
 * License: GPL-2.0+
 */

/* ---------------------------------------------------------------------------
 * WPForms - Trigger dataLayer event on successful form submission
 * --------------------------------------------------------------------------- */

add_action(
	'wpforms_process_complete',
	function( $fields, $entry, $form_data, $entry_id ) {
		?>
		<script>
			window.dataLayer = window.dataLayer || [];

			window.dataLayer.push({
				'event': 'wpf_submission',
				'formId': <?php echo $form_data['id']; ?>,
				'formLabel': '<?php echo $form_data['settings']['form_title']; ?>',
			});
		</script>
		<?php
	},
	10,
	4
);


/* ---------------------------------------------------------------------------
 * Require WPForms or WPForms Lite plugin to be active
 * --------------------------------------------------------------------------- */

// Automatically activate WPForms plugin if needed
register_activation_hook(
	__FILE__,
	function() {
		$wpforms_plugin      = 'wpforms/wpforms.php';
		$wpforms_lite_plugin = 'wpforms-lite/wpforms.php';

		if ( file_exists( WP_PLUGIN_DIR . '/' . $wpforms_plugin ) && ! is_plugin_active( $wpforms_plugin ) ) {
			activate_plugin( $wpforms_plugin );

		} elseif ( file_exists( WP_PLUGIN_DIR . '/' . $wpforms_lite_plugin ) && ! is_plugin_active( $wpforms_lite_plugin ) ) {
			activate_plugin( $wpforms_lite_plugin );

		}
	}
);

// Prevent plugin activation if WPForms is not installed or is blocked by WP Local Toolbox
add_action(
	'admin_init',
	function() {
		// Note: Use register_activation_hook to activate WPForms plugin if installed.
		//	   Attempting to activate here will work, but won't show the plugin as active until the next page load.

		$wpforms_plugin      = 'wpforms/wpforms.php';
		$wpforms_lite_plugin = 'wpforms-lite/wpforms.php';

		if ( current_user_can( 'activate_plugins' ) && ! is_plugin_active( $wpforms_plugin ) && ! is_plugin_active( $wpforms_lite_plugin ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );

			add_action(
				'admin_notices',
				function() {
					unset( $_GET['activate'] );

					echo '<div class="notice notice-error is-dismissible"><p>Plugin cannot be activated. Please install and activate WPForms plugin first.</p></div>';
				}
			);
		}
	}
);

// Automatically deactivate if WPForms plugin is deactivated
add_action(
	'deactivated_plugin',
	function( $plugin, $network_activation ) {
		$wpforms_plugin      = 'wpforms/wpforms.php';
		$wpforms_lite_plugin = 'wpforms-lite/wpforms.php';

		if ( $plugin === $wpforms_plugin || $plugin === $wpforms_lite_plugin ) {
			add_action(
				'update_option_active_plugins',
				function() {
					deactivate_plugins( plugin_basename( __FILE__ ) );
				}
			);
		}
	},
	10,
	2
);


/* ---------------------------------------------------------------------------
 * Check for update
 * --------------------------------------------------------------------------- */

add_filter(
	'update_plugins_bitbucket.org',
	function( $update, $plugin_data, $plugin_file, $locales ) {
		static $raw_response = false; // Prevent duplicate requests

		if (
			plugin_basename( __FILE__ ) !== $plugin_file ||
			empty( $plugin_data['UpdateURI'] ) ||
			! empty( $update )
		) {
			return $update;
		}

		if ( false === $raw_response ) {
			$raw_response = wp_remote_get(
				$plugin_data['UpdateURI'],
				array(
					'timeout' => 10,
					'headers' => array(
						'Accept' => 'application/json',
					),
				)
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $raw_response ), true );

		// error_log( print_r( $response, false ), 3, __DIR__ .'/log.txt' );

		if ( ! empty( $response ) && ! empty( $response[ $plugin_file ] ) ) {
			return $response[ $plugin_file ];
		}

		return $update;
	},
	10,
	4
);
